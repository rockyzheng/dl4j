package md.midday;

import me.midday.Perceptron;
import me.midday.matrix.Matrix;
import me.midday.matrix.MatrixException;

/**
 * Created by wuzheng on 17-11-10.
 */
public class TestPerceptron {
    public static void main(String[] args) throws MatrixException {

        Matrix trainMat = new Matrix(4, 2);
        trainMat.setValue(1.0, 0, 0);
        trainMat.setValue(1.0, 0, 1);
        trainMat.setValue(0, 1, 0);
        trainMat.setValue(0, 1, 1);
        trainMat.setValue(1, 2, 0);
        trainMat.setValue(0, 2, 1);
        trainMat.setValue(0, 3, 0);
        trainMat.setValue(1, 3, 1);

        Matrix label = new Matrix(1, 4);
        label.setValue(1, 0, 0);
        label.setValue(0, 0, 1);
        label.setValue(0, 0, 2);
        label.setValue(0, 0, 3);

        Perceptron perceptron = new Perceptron(2);
        System.out.println(trainMat);

        perceptron.train(trainMat, label, 100, 0.01);
    }

}
