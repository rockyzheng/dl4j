package me.midday.matrix;

/**
 * Created by wuzheng on 17-11-9.
 */
public class Shape {
    private int rows;
    private int cols;

    public Shape(int rows, int cols){
        this.rows = rows;
        this.cols = cols;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Shape){
            return this.rows == ((Shape) obj).rows  && this.cols == ((Shape) obj).cols;
        }
        return false;
    }

    @Override
    public int hashCode() {
        // todo over write
        return super.hashCode();
    }
}
