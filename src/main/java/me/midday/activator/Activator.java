package me.midday.activator;

/**
 * Created by wuzheng on 17-11-9.
 */
public interface Activator {
    double active(double z);

    double derivative(double z);
}
