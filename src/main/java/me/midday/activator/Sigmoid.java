package me.midday.activator;

import java.util.Map;

/**
 * Created by wuzheng on 17-11-9.
 */
public class Sigmoid implements Activator{

    public Sigmoid(){}

    @Override
    public double active(double z) {
        double a = 1 / (1 + Math.exp(-1 * z));
        return a;
    }

    @Override
    public double derivative(double z) {
        double d = active(z) * (1-active(z));
        return d;
    }

}
